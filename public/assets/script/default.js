'use strict';
//Object.assign-Polyfill
if (typeof Object.assign != 'function') {
  Object.assign = function (target, varArgs) { // .length of function is 2
    if (target == null) { // TypeError if undefined or null
      throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
      var nextSource = arguments[index];

      if (nextSource != null) { // Skip over if undefined or null
        for (var nextKey in nextSource) {
          // Avoid bugs when hasOwnProperty is shadowed
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  };
}

/*
 * 動態引入CSS
 */
var stylePath = "https://tw.hicdn.beanfun.com/beanfun/GamaWWW/allProducts/style/gbox/";
var link = document.createElement('link');
link.type = 'text/css';
link.rel = "stylesheet";
// link.href = stylePath + 'gbox.css';
link.href = 'assets/style/gbox.css';
document.head.appendChild(link);

function popup () {
  
  var _isString = function _isString (value) {
    return typeof value === "string";
  },
    _isFunction = function _isFunction (value) {
      return typeof value === "function";
    },
    _isNumber = function _isNumber (value) {
      return typeof value === "number";
    },
    _isUndefined = function _isUndefined (value) {
      return typeof value === "undefined";
    },
    _isObject = function _isObject (value) {
      return typeof value === "object";
    },
    _isNotFalse = function _isNotFalse (value) {
      return value !== false;
    },
    _isFuncOrString = function _isFuncOrString (value) {
      return _isFunction(value) || _isString(value);
    };

  /*
   * 設置參數、狀態
   */
  var _actionBtn = [{
    text: '確定',
    id: null,
    class: null,
    click: null //網址 or Function
  }];
  var _defaults = {
    titleText: null,
    addClass: null,
    hasCloseBtn: true,
    closeBtn: '\u00D7', //可插入HTML
    hasActionBtn: true,
    actionBtn: _actionBtn,
    fixedPos: true,
    afterClose: null, //網址 or Function
    afterOpen: null //function
  };
  var _isOpen = false;

  /*
   * gbox結構
   */
  var createPopupBox = function (setting) {
    _isOpen = true;
    // document.createElement寫成fn重複使用!?
    /*---------外層容器---------*/
    var popupBox_outter = document.createElement('div');
    popupBox_outter.id = 'gbox';
    popupBox_outter.classList = 'gbox';
    //額外加上class
    if (setting.option.addClass) {
      popupBox_outter.classList.add(setting.option.addClass);
    }
    /*---------遮罩---------*/
    var popupBox_module = document.createElement('div');
    popupBox_module.classList = 'gbox-module';
    popupBox_outter.appendChild(popupBox_module);

    /*---------內層容器---------*/
    var popupBox_wrap = document.createElement('div');
    popupBox_wrap.classList = 'gbox-wrap';
    popupBox_outter.appendChild(popupBox_wrap);

    /*---------標題區塊---------*/
    if (setting.option.titleText) {
      var popupBox_title = document.createElement('div');
      popupBox_title.classList = 'gbox-title';
      popupBox_title.textContent = setting.option.titleText;
      popupBox_wrap.appendChild(popupBox_title);
    }
    /*---------內容區塊---------*/
    //帶入內容   
    var popupBox_content = document.createElement('div');
    popupBox_content.classList = 'gbox-content';
    popupBox_content.innerHTML = setting.content;
    popupBox_wrap.appendChild(popupBox_content);

    /*---------關閉按鈕---------*/
    if (setting.option.hasCloseBtn) {
      var popupBox_closeBtn = document.createElement('button');
      popupBox_closeBtn.classList = 'gbox-close';
      popupBox_closeBtn.innerHTML = setting.option.closeBtn;
      popupBox_closeBtn.addEventListener('click',destroyPopupBox);
      popupBox_wrap.appendChild(popupBox_closeBtn);
    }

    /*---------按鈕區塊---------*/
    if (setting.option.hasActionBtn) {
      var popupBox_action = document.createElement('div');
      popupBox_action.classList = 'gbox-action';
      //生成按鈕
      setting.option.actionBtn.forEach(function (btn) {
        var actionBtn = document.createElement('a');
        actionBtn.classList = 'gbox-btn';
        btn.id && (actionBtn.id = btn.id);
        btn.text && (actionBtn.textContent = btn.text);
        btn.class && actionBtn.classList.add(btn.class);
        //綁定按鈕事件(如果是網址)
        if (_isFunction(btn.click)) {
          actionBtn.addEventListener('click', function () {
            btn.click();
          })
        } else if (_isString(btn.click)) {
          actionBtn.href = btn.click;
        } else {
          actionBtn.addEventListener('click', destroyPopupBox)
        }
        popupBox_action.appendChild(actionBtn);
      });
      popupBox_wrap.appendChild(popupBox_action);
    }

    /*---------Open---------*/
    document.body.appendChild(popupBox_outter);

    /*---------opened callback---------*/
    if (_isFunction(setting.option.afterOpen)) {
      setting.option.callback();
    }

    /*---------closed callback---------*/

  };


  var destroyPopupBox = function (callback) {
    var popupBox = document.getElementById('gbox');
    popupBox.parentNode.removeChild(popupBox);
    if (_isFunction(callback)) {
      callback();
    }
    _isOpen = false;
  };

  //開啟
  this.open = function (content, option) {
    // var o_c = _isUndefined(content) ? '沒有內容' : content;
    // var o_o = _isUndefined(option) ? _defaults : Object.assign({},_defaults,option);
    var setting = {
      content: _isUndefined(content) ? '沒有內容' : content,
      option: _isUndefined(option) ? _defaults : Object.assign({}, _defaults, option)
    };
    _isOpen || createPopupBox(setting)

    /*------鎖定畫面(未完成)---------*/

    /*------一版到底(未完成)---------*/

  }
  //關閉
  this.close = function () {
    destroyPopupBox();
  }
}

var gbox = new popup();


//gbox.open('文字',參數);
// gbox.close();
// gbox.open();
// JSON.parse(JSON.stringify(theObject))
